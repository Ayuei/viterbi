from max_ent import main as get_clf
from max_ent import get_features_from_file
import sys
import numpy as np
from sklearn.metrics import accuracy_score, recall_score, f1_score, precision_score
from multiprocessing import Process, Manager, cpu_count
import math

tag_set = []


def get_prob(clf, w, prev_pred, tag):
    w['previous_class'] = prev_pred
    tag_ind = tag_set.index(tag)
    probs = clf.predict_proba(w)[0]

    return probs[tag_ind]


def multi_processing_wrapper(L, clf, sents, process_id):
    for sent in sents:
        tmp = viterbi(clf, sent)
        L[process_id].append(tmp)

    L[process_id] = list(L[process_id])


def unwrap(lst):
    temp = []

    for l in lst:
        temp.extend(l)

    lst = temp

    return lst


def viterbi(clf, sent):
    deltas = np.zeros((len(sent), len(tag_set)), dtype=np.float64)
    # Initial probs
    deltas[0] = np.array(clf.predict_proba(sent[0])[0])
    phi = np.zeros((len(sent), len(tag_set)), dtype=np.int8)

    for i in range(1, len(sent)):
        for j, tag in enumerate(tag_set):
            deltas[i][j], phi[i][j] = max((deltas[i-1][p_tag_indx] +
                                           get_prob(clf, sent[i], prev_tag, tag), p_tag_indx)
                                           for p_tag_indx, prev_tag in enumerate(tag_set))

    t = np.array([0 for i in range(len(sent))], dtype=np.int8)

    t[-1] = np.argmax(deltas[-1])

    for i in reversed(range(len(phi)-1)):
        t[i] = phi[i+1][t[i+1]]

    tags_to_return = []
    for tag in t:
        tags_to_return.append(tag_set[tag])

    return np.array(tags_to_return, dtype=object)


def main():
    clf = get_clf()

    global tag_set
    tag_set = list(clf.classes_)

    print(clf.classes_)
    preds = []
    x_test, y_test, sentences = get_features_from_file(open('data/eng.testb', 'r'))

    with Manager() as manager:
        L = manager.list([manager.list() for i in range(cpu_count())])
        processes = []
        l = sentences
        n = int(math.ceil(len(sentences)/cpu_count()))
        chunks = [l[i:i + n] for i in range(0, len(l), n)]
        for i in range(cpu_count()):
            p = Process(target=multi_processing_wrapper, args=(L, clf, chunks[i], i))
            p.start()
            processes.append(p)

        for p in processes:
            p.join()

        preds = list(L)

    preds = unwrap(preds)

    f = open('data/predictions.txt', 'w+')
    trth_count = 0
    predz = []
    for sent, pred in zip(sentences, preds):
        for w, p in zip(sent, pred):
            s = w['word']+' '+str(p)+' '+str(y_test[trth_count])+'\n'
            f.write(s)
            trth_count += 1
        f.write('\n')
        predz.extend(pred)
    metrics = [accuracy_score, recall_score, f1_score, precision_score]

    for metric in metrics:
        if not (str(metric.__name__) == 'accuracy_score' or str(metric.__name__) == 'confusion_matrix'):
            print(metric.__name__, ":", metric(y_test, predz, average="macro"))
        else:
            print(metric.__name__, ":", metric(y_test, predz))

    return 0


if __name__ == "__main__":
    sys.exit(main())

